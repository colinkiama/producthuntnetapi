﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Interfaces
{
    interface ITopicable
    {
        string Id { get; set; }
    }
}
