﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Interfaces
{
    public interface IVotable
    {
        string Id { get; set; }
        bool IsVoted { get; set; }
        int VotesCount { get; set; }
    }
}
