﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Enums
{
    public enum PostsQueryParameter
    {
        PostedAfter,
        PostedBefore,
        Topic,
        TwitterUrl
    }
}
