﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Enums
{
    public enum QueryParameter
    {
        After,
        Before,
        First,
        Last,
        Order
    }
}
