﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Model
{
    public class QueryResult<T>
    {
        [JsonProperty("edges")]
        public Edge<T> Edges { get; set; }

        [JsonProperty("pageInfo")]
        public PageInfo ResultPageInfo { get; set; }

        [JsonProperty("totalCount")]
        public int TotalCount { get; set; }
    }
}
