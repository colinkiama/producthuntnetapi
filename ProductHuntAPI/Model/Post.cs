﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using ProductHuntAPI.Interfaces;
namespace ProductHuntAPI.Model
{
    public class Post : IVotable, ITopicable
    {
        [JsonProperty("commentsCount")]
        public int CommentsCount { get; set; }
        
        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }
        
        [JsonProperty("description")]
        public string Desription { get; set; }
        
        [JsonProperty("featuredAt")]
        public DateTime FeaturedAt { get; set; }
        
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("isCollected")]
        public bool IsCollected { get; set; }
        
        [JsonProperty("isVoted")]
        public bool IsVoted { get; set; }
        
        //public List<User> Makers { get; set; }
        [JsonProperty("reviewsRating")]
        public float ReviewsRating { get; set; }
        
        [JsonProperty("slug")]
        public string Slug { get; set; }
        
        [JsonProperty("tagline")]
        public string Tagline { get; set; }

        //[JsonProperty("thumbnail")]
        //public Media Thumbnail { get; set; }

        [JsonProperty("url")]
        public string PostUrl { get; set; }
        
        [JsonProperty("userId")]
        public string UserId { get; set; }
        
        [JsonProperty("votesCount")]
        public int VotesCount { get; set; }
        
        [JsonProperty("website")]
        public string Website { get; set; }
    }
}
