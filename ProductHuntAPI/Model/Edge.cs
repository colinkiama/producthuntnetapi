﻿using Newtonsoft.Json;

namespace ProductHuntAPI.Model
{
    public class Edge<T>
    {
        [JsonProperty("cursor")]
        public string Cursor { get; set; }
        
        [JsonProperty("node")]
        public T Node { get; set; }
    }
}