﻿using ProductHuntAPI.Enums;
using ProductHuntAPI.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using ProductHuntAPI.Helpers;

namespace ProductHuntAPI.Model
{
    public class Query
    {
        public string FieldName { get; private set; }
        protected Dictionary<string, object> _parameterDictionary = new Dictionary<string, object>();
        public List<Query> ChildQueries = new List<Query>();

        public Query(string fieldName)
        {
            FieldName = fieldName;
        }
       
        public Query AddParameter(QueryParameter paramName, object paramValue)
        {
            string paramString = QueryParameterHelper.Instance.CreateApiFieldFromQueryParameterEnum(paramName);
            _parameterDictionary.Add(paramString, paramValue);
            return this;
        }

        public Query AddChildQuery(Query queryToAdd)
        {
            ChildQueries.Add(queryToAdd);
            return this;
        }
    }
}
