﻿using ProductHuntAPI.Enums;
using ProductHuntAPI.Helpers;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Model
{
    public class PostQuery : Query
    {
        public PostQuery(string fieldName) : base(fieldName)
        {
        }

        public PostQuery AddParameter(PostsQueryParameter paramName, object paramValue)
        {
            string paramString = QueryParameterHelper.Instance.CreateApiFieldFromQueryParameterEnum(paramName);
            _parameterDictionary.Add(paramString, paramValue);
            return this;
        }
    }
}
