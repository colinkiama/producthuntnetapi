﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductHuntAPI.Helpers
{
    public sealed class QueryParameterHelper
    {
        private static Lazy<QueryParameterHelper> lazy = new Lazy<QueryParameterHelper>(() => new QueryParameterHelper());
        public static QueryParameterHelper Instance = lazy.Value;
        private QueryParameterHelper() { }

        public string CreateApiFieldFromQueryParameterEnum(Enum enumToConvert)
        {
            string enumAsString = enumToConvert.ToString();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < enumAsString.Length; i++)
            {
                char charToAdd = enumAsString[i];
                if (i == 0)
                {
                    sb.Append(char.ToLower(charToAdd));
                }
                else
                {
                    sb.Append(charToAdd);
                }
            }
            return sb.ToString();
        }

    }
}
